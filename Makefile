FILES := cv_en.tex
READER := evince

all: $(FILES:.tex=.pdf) clean

%.pdf: %.tex
	pdflatex $<
	pdflatex $<

clean:
	rm -f $(FILES:.tex=.aux) $(FILES:.tex=.log) $(FILES:.tex=.out) $(FILES:.tex=.toc)

distclean: clean
	rm -f $(FILES:.tex=.pdf)

display: all
	$(READER) $(FILES:.tex=.pdf)
